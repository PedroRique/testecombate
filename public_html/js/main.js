$(document).ready(function(){
  $('.slickSlide').slick({
    infinite: false,
    slidesToShow: 4,
     responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 379,
      settings: {
        slidesToShow: 1.5,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
  });
});